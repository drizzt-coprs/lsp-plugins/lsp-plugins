Name:		lsp-plugins
Version:	1.1.9
Release:	0%{?dist}
Summary:	Collection of free plugins compatible with LADSPA, LV2 and LinuxVST

License:	LGPLv3
URL:		https://lsp-plug.in
Source0:	https://github.com/sadko4u/%{name}/archive/%{name}-%{version}.tar.gz

BuildRequires:	gcc gcc-c++
BuildRequires:	lv2-devel
BuildRequires:	jack-audio-connection-kit-devel
BuildRequires:	libsndfile-devel
BuildRequires:	cairo-devel
BuildRequires:	ladspa-devel
BuildRequires:	expat-devel
BuildRequires:	mesa-libGLU-devel
BuildRequires:	mesa-libGL-devel
BuildRequires:	php-cli

%description
LSP (Linux Studio Plugins) is a collection of open-source plugins
currently compatible with LADSPA, LV2 and LinuxVST formats.

%prep
%setup -q -n %{name}-%{name}-%{version}
# removing RPATH
sed -e 's/-Wl,-rpath,$(LD_PATH)//' \
    -i scripts/make/tools.mk

%build
CFLAGS="$RPM_OPT_FLAGS" CXXFLAGS="$RPM_OPT_FLAGS" LDFLAGS="$RPM_LD_FLAGS" make %{?_smp_mflags}

%install
%make_install BIN_PATH=%{_bindir} LIB_PATH=%{_libdir} DOC_PATH=%{_docdir}

%files
%doc CHANGELOG.txt README.txt
%license LICENSE.txt
%{_bindir}/*
%{_libdir}/*
%{_datadir}/*

%changelog
* Wed Jun 26 2019 Timothy Redaelli <tredaelli@redhat.com> - 1.1.9-0
- Initial release


